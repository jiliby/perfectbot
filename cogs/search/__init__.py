from .search import Search
from discord.ext.commands import errors


async def setup(bot):
    """setup function"""
    from setup import config
    if None in [config["google_api_key"], config["custom_search_engine"]]:
        raise errors.ExtensionError(
            "Custom search engine configuration not found", name="cogs.music")
    await bot.add_cog(Search(bot))
