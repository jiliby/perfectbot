# import
import random
import urllib.parse
import json
import discord
from discord.ext import commands

from setup import config, COLOR
from utils.http import get_aiohttp
from utils.messages import something_went_wrong

google_api_key = config["google_api_key"]
custom_search_engine = config["custom_search_engine"]


class Search(commands.Cog):
    "Поиск в интернете"

    def __init__(self, bot):
        self.bot = bot

    @commands.command(aliases=["g", "search", "гугл"])
    async def google(self, ctx, *, query):
        """Поиск в Гугле"""
        content = await get_aiohttp(f"https://www.googleapis.com/customsearch/v1?q={urllib.parse.quote_plus(query)}&start=1&key={google_api_key}&cx={custom_search_engine}")
        if content.code != 200:
            return await something_went_wrong(ctx)
        data = json.loads(content.text)
        if data["items"] and len(data["items"]) > 0:
            embed = discord.Embed(color=COLOR)
            for i in range(3):
                embed.add_field(
                    name=f'{i+1}. {data["items"][i]["title"]}',
                    value=f'[{data["items"][i]["link"]}]({data["items"][i]["link"]})',
                    inline=False,
                )
            embed.set_footer(text='Запрос: "' + query + '"')
            await ctx.send(
                f"{ctx.message.author.mention}, вот что мне удалось найти:",
                embed=embed,
            )
        else:
            return await ctx.send(
                f"{ctx.message.author.mention} :thinking: Интернет не в курсе, поищите что-то другое."
            )

    @commands.command(aliases=["i", "img", "pic", "картинка"])
    async def image(self, ctx, *, query):
        """Поиск картинок в Гугле"""
        content = await get_aiohttp(f"https://www.googleapis.com/customsearch/v1?q={urllib.parse.quote_plus(query)}&start=1&key={google_api_key}&cx={custom_search_engine}&searchType=image")
        if content.code != 200:
            return await something_went_wrong(ctx)
        data = json.loads(content.text)
        if data["items"] and len(data["items"]) > 0:
            amount = len(data["items"])
            embed = discord.Embed(color=COLOR)
            item = random.randint(0, amount - 1)
            embed.set_image(url=data["items"][item]["link"])
            embed.set_footer(text='Запрос: "' + query + '"')
            await ctx.send(
                f"{ctx.message.author.mention}, вот что мне удалось найти:",
                embed=embed,
            )
        else:
            return await ctx.send(
                f"{ctx.message.author.mention} :thinking: Интернет не в курсе, поищите что-то другое."
            )
