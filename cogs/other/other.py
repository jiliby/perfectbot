# imports
import typing
import random
import discord
from discord.ext import commands
from bs4 import BeautifulSoup
from utils.http import get_aiohttp
from utils.messages import something_went_wrong
import requests
import urllib.parse
from setup import COLOR
from typing import Optional

random.seed()


class Other(commands.Cog):
    "Разные команды"

    def __init__(self, bot):
        self.bot = bot

    # commands
    @commands.command(aliases=["пинг"])
    async def ping(self, ctx):
        """🏓"""
        await ctx.send("🏓 Pong: **{}ms**".format(round(self.bot.latency * 1000, 2)))

    @commands.command(aliases=["эхо"])
    async def echo(self, ctx, *, arg):
        """:repeat: Повторяю за тобой"""
        if ctx.message.channel.guild.me.guild_permissions.manage_messages:
            await ctx.message.delete()
        await ctx.send(arg)

    @commands.command(aliases=["ролл", "кручение"])
    async def roll(self, ctx, num: typing.Optional[int] = 100):
        """Получить случайное число в промежутке от 1 до 100 (если не задано другое)

        Args:
            num (int, optional): верхняя граница случайного числа, по умолчанию равна 100.
        """
        if isinstance(num, int) and num > 0:
            await ctx.send(random.randint(0, num))
        else:
            await ctx.send("Простите, что?")

    @commands.command(aliases=["бан"])
    @commands.has_permissions(ban_members=True)
    async def ban(
        self,
        ctx: commands.Context,
        members: commands.Greedy[discord.Member],
        delete_days: typing.Optional[int] = 0,
        *,
        reason: str
    ):
        """Ban users and delete their messeges (optional)

        Args:
            members (commands.Greedy[discord.Member]): those who will be banned
            reason (str): reason
            delete_days (typing.Optional[int], optional): deletes messages for this time period (in days). Defaults to 0.
        """
        for member in members:
            await member.ban(delete_message_days=delete_days, reason=reason)
            await ctx.send(f"Пользователь **{member}** был забанен по причине *{reason}*")
        await ctx.message.delete()
            
    @commands.command(aliases=['кик', 'выгнать'])
    @commands.has_permissions(kick_members=True)
    async def kick(
        self,
        ctx: commands.Context,
        members: commands.Greedy[discord.Member],
        *,
        reason: str
    ):
        """
        Kick member from server and delete their messeges (optional)

        Args:
            members (commands.Greedy[discord.Member]): those who will be kicked out
            reason (str): reason
            delete_days (typing.Optional[int], optional): deletes messages for this time period (in days). Defaults to 0.
        """
        for member in members:
            await member.kick(reason=reason)
            await ctx.send(f"Пользователь **{member}** был выгнан по причине *{reason}*")
        await ctx.message.delete()
        
        
    @commands.command()
    @commands.is_owner()
    async def purge(self, ctx, limit=1):
        """
        Удаляет выбранное количество сообщений в канале
        (по умолчанию удаляет только предыдущее)
        """
        try:
            await ctx.channel.purge(limit=limit + 1)
        except AttributeError:
            pass

    @commands.command(aliases=["шар"])
    async def ball(self, ctx):
        """:8ball: Спросить магический шар, предсказывающий будущее."""
        messages = [
            ":8ball: Несомненно.",
            ":8ball: Это решительно так.",
            ":8ball: Без сомнения.",
            ":8ball: Определенно да.",
            ":8ball: Можешь на это рассчитывать.",
            ":8ball: Насколько я понимаю, да.",
            ":8ball: Скорее всего.",
            ":8ball: Прогноз хороший.",
            ":8ball: Да.",
            ":8ball: Знаки указывают на то, что да.",
            ":8ball: Ответ туманный, попробуйте еще раз.",
            ":8ball: Спросите еще раз позже.",
            ":8ball: Лучше не говорить тебе сейчас.",
            ":8ball: Невозможно предсказать сейчас.",
            ":8ball: Сконцентрируйся и спроси еще раз.",
            ":8ball: Не рассчитывай на это.",
            ":8ball: Ответ отрицательный",
            ':8ball: Мои источники говорят "нет"',
            ":8ball: Прогноз не очень хороший.",
            ":8ball: Очень сомнительно.",
        ]
        await ctx.send(messages[random.randint(0, len(messages) - 1)])

    @commands.command(aliases=["монетка"])
    async def coin(self, ctx):
        """:coin: Подбросить монетку"""
        if random.randint(0, 1) == 1:
            await ctx.send(":coin: Орёл!")
        else:
            await ctx.send(":coin: Решка!")

    @commands.command(pass_context=True, aliases=["соус"])
    async def sauce(self, ctx: commands.Context, link: Optional[str], similarity: Optional[int] = 75):
        """
        Ищем соус картинки, точность по умолчанию 75%
        (картинку можно просто прилепить к сообщению или по url)
        """
        source = None
        file = ctx.message.attachments
        if len(file) == 0: # if the message doesn't contain any attachments
            if link is None:
                await ctx.message.delete(delay=15)
                return await ctx.reply("А где картинка то?", mention_author=True, delete_after=15)
            url = link
        else:
            url = file[0].url
            if link is not None:    # if image is message attachment
                similarity = link   # try to use first arg as search similarity
        
        try:
            similarity = int(similarity) # if similarity is not int
        except ValueError:
            return await ctx.reply(":warning: Неверно указана точность поиска.")

        content = await get_aiohttp(f"http://saucenao.com/search.php?url={url}")
        if content.code != 200:
            return await something_went_wrong(ctx)
        soup = BeautifulSoup(content.text, "html.parser")
        for result in soup.select(".resulttablecontent"):
            if similarity > float(result.select(".resultsimilarityinfo")[0].contents[0][:-1]):
                break
            if result.select("a"):
                source = result.select("a")[0]["href"]
                return await ctx.reply(f"<{source}>", mention_author=True)
                # todo: заменить на embedded
        if source is None:
            return await ctx.reply(
                ":confused: С заданным показателем точности ничего не найдено",
                mention_author=True,
            )

    @commands.command(pass_context=True)
    async def xkcd(self, ctx, *, comic=""):
        """Достаём комикс xkcd."""
        if comic == "random" or comic == "рандом":
            randcomic = requests.get("https://c.xkcd.com/random/comic/")
            comic = randcomic.url.split("/")[-2]
        site = requests.get(f"https://xkcd.com/{comic}/info.0.json")
        if site.status_code == 404:
            site = None
            found = None
            search = urllib.parse.quote(comic)
            result = (requests.get(
                f"https://www.google.com/search?&q={search}+site:xkcd.com")
            ).text
            soup = BeautifulSoup(result, "html.parser")
            links = soup.find_all("cite")
            for link in links:
                if link.text.startswith("https://xkcd.com/"):
                    found = link.text.split("/")[3]
                    break
            if not found:
                await ctx.send(":confused: Такого комикса нету")
            else:
                site = requests.get(f"https://xkcd.com/{found}/info.0.json")
                comic = found
        if site:
            content = site.json()
            embed = discord.Embed(
                title=f"xkcd {content['num']}: {content['title']}",
                url=f"https://xkcd.com/{comic}",
                color=COLOR,
            )
            embed.set_image(url=content["img"])
            embed.set_footer(text=f"{content['alt']}")
            await ctx.send("", embed=embed)
