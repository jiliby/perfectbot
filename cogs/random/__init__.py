from .random import Random as Random_cog


async def setup(bot):
    """setup function"""
    await bot.add_cog(Random_cog(bot))
