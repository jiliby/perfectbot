# import
from random import randint
import discord
from discord.ext import commands

from utils.http import randomimage, nekoslife, reddit_radnom_hot
from utils.messages import something_went_wrong
from setup import COLOR


class Random(commands.Cog):
    """
    RANDOM BULSHIT GO!!!
    """

    def __init__(self, bot):
        self.bot = bot

    @commands.command(aliases=["кот", "🐱"])
    async def cat(self, ctx: commands.Context):
        """Постим котиков 🐱"""
        if randint(0, 1) == 0:  # используем случайное апи
            await randomimage(ctx, "cat")
        else:
            embed = await nekoslife(ctx, "meow")
            if embed is None:
                await something_went_wrong(ctx)
            else:
                await ctx.send("", embed=embed)

    @commands.command(aliases=[":dog:", "🐶"])
    async def dog(self, ctx: commands.Context):
        """Постим собак 🐶"""
        if randint(0, 1) == 0:  # используем случайное апи
            await randomimage(ctx, "dog")
        else:
            embed = await nekoslife(ctx, "woof")
            if embed is None:
                await something_went_wrong(ctx)
            else:
                await ctx.send("", embed=embed)

    @commands.command(aliases=["bird", "птица", "птиц", "🐦"])
    async def birb(self, ctx: commands.Context):
        """Постим птиц 🐦"""
        await randomimage(ctx, "birb")

    @commands.command(aliases=["лис", "лиса", "🦊"])
    async def fox(self, ctx: commands.Context):
        """Постим лис 🦊"""
        await randomimage(ctx, "fox")

    @commands.command(aliases=["гусь"])
    async def goose(self, ctx: commands.Context):
        """Постим гуся"""
        embed = await nekoslife(ctx, "goose")
        if embed is None:
            await something_went_wrong(ctx)
        else:
            await ctx.send("", embed=embed)

    @commands.command(aliases=["ящурка", "🦎"])
    async def lizard(self, ctx: commands.Context):
        """Постим ящурок 🦎"""
        embed = await nekoslife(ctx, "lizard")
        if embed is None:
            await something_went_wrong(ctx)
        else:
            await ctx.send("", embed=embed)

    @commands.command(aliases=["forg", "легушка", "🐸"])
    async def frog(self, ctx: commands.Context):
        """Постим легущек 🐸"""
        em = discord.Embed(color=COLOR)
        tmp = randint(0, 54)
        em.set_image(
            url=f"http://www.allaboutfrogs.org/funstuff/random/00{tmp:02d}.jpg"
        )
        await ctx.send("", embed=em)

    @commands.command(aliases=["мем"])
    async def meme(self, ctx: commands.Context):
        """Постим мемы с r/dankmemes"""
        post = await reddit_radnom_hot(ctx, "dankmemes")
        if type(post) is str:
            await ctx.send(post)
        elif type(post) is discord.Embed:
            await ctx.send("", embed=post)
        else:
            await something_went_wrong(ctx)
