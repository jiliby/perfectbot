import json
from datetime import datetime
from urllib.parse import quote

import discord
from discord.ext import commands
from utils.http import get_aiohttp
from setup import COLOR

url = "https://store-site-backend-static.ak.epicgames.com/freeGamesPromotions"


class EGS(commands.Cog):
    """
    Получить информацию о актуальных раздачах халявы на EGS
    """

    def __init__(self, bot):
        self.bot = bot

    @commands.command(aliases=["егс", "халява", "игры", "games"])
    async def egs(self, ctx):
        """
        Получить еженедельную халяву из EGS
        """
        now = datetime.now()
        res = await get_aiohttp(url)
        if res.code != 200:
            return await ctx.send(
                f"{ctx.message.author.mention}:confused: Cервис не отвечает."
            )
        result = json.loads(res.text)

        await ctx.send("Сейчас в раздаче следующие игры:")
        for item in result["data"]["Catalog"]["searchStore"]["elements"]:
            try:
                discountPercentage = item["promotions"]["promotionalOffers"][0][
                    "promotionalOffers"
                ][0]["discountSetting"]["discountPercentage"]
            except Exception:
                continue
            else:
                if discountPercentage == 0:
                    start_date = datetime.strptime(
                        item["promotions"]["promotionalOffers"][0]["promotionalOffers"][
                            0
                        ]["startDate"],
                        "%Y-%m-%dT%H:%M:%S.%fZ",
                    )
                    end_date = datetime.strptime(
                        item["promotions"]["promotionalOffers"][0]["promotionalOffers"][
                            0
                        ]["endDate"],
                        "%Y-%m-%dT%H:%M:%S.%fZ",
                    )
                    if now < start_date and now > end_date:
                        continue
                    embed = discord.Embed(
                        title=item["title"],
                        url=f"https://www.epicgames.com/store/ru/product/{quote(item['productSlug'])}/home",
                        description=item["description"],
                        color=COLOR,
                    )
                    embed.set_image(
                        url=quote(item["keyImages"][2]["url"], safe="http://")
                    )

                    embed.add_field(
                        name="Начало:",
                        value=start_date.strftime("%m/%d/%Y, %H:%M:%S"),
                        inline=True,
                    )
                    embed.add_field(
                        name="Конец:",
                        value=end_date.strftime("%m/%d/%Y, %H:%M:%S"),
                        inline=True,
                    )

                    await ctx.send("", embed=embed)
