# import
from random import randint
import asyncio
import discord
from discord.ext import commands

from utils.http import nekoslife, reddit_radnom_hot
from utils.messages import something_went_wrong, nsfw_only
from utils.checks import dm_or_nsfw


class NSFW(commands.Cog):
    """
    Только для NSFW-каналов!
    """

    def __init__(self, bot):
        self.bot = bot

    @commands.command(aliases=["хентай"])
    @dm_or_nsfw()
    async def hentai(self, ctx: commands.Context):
        """Random hentai"""
        source = ["classic", "Random_hentai_gif"]
        if ctx.channel.type is discord.ChannelType.private or ctx.channel.is_nsfw():
            embed = await nekoslife(ctx, f"{source[randint(0, 1)]}")
            if embed is None:
                return something_went_wrong(ctx)
            else:
                return await ctx.send("", embed=embed)
        else:
            await nsfw_only(ctx)

    @commands.command()
    @dm_or_nsfw()
    async def yuri(self, ctx: commands.Context):
        """Random yuri"""
        if ctx.channel.type is discord.ChannelType.private or ctx.channel.is_nsfw():
            embed = await nekoslife(ctx, "yuri")
            if embed is None:
                return await ctx.reply(":confused: Что-то пошло не так.")
            else:
                return await ctx.send("", embed=embed)
        else:
            await nsfw_only(ctx)

    @commands.command()
    @dm_or_nsfw()
    async def tits(self, ctx: commands.Context):
        """Random hentai tits"""
        if ctx.channel.type is discord.ChannelType.private or ctx.channel.is_nsfw():
            embed = await nekoslife(ctx, "tits")
            if embed is None:
                return await ctx.reply(":confused: Что-то пошло не так.")
            else:
                return await ctx.send("", embed=embed)
        else:
            await nsfw_only(ctx)

    @commands.command()
    @dm_or_nsfw()
    async def feet(self, ctx: commands.Context):
        """Тут есть фут-фетишисты???"""
        if ctx.channel.type is discord.ChannelType.private or ctx.channel.is_nsfw():
            embed = await nekoslife(ctx, "feet")
            if embed is None:
                return await ctx.reply(":confused: Что-то пошло не так.")
            else:
                return await ctx.send("", embed=embed)
        else:
            await nsfw_only(ctx)

    @dm_or_nsfw()
    async def anal(self, ctx: commands.Context):
        """Anal hentai"""
        if ctx.channel.type is discord.ChannelType.private or ctx.channel.is_nsfw():
            embed = await nekoslife(ctx, "anal")
            if embed is None:
                return await ctx.reply(":confused: Что-то пошло не так.")
            else:
                return await ctx.send("", embed=embed)
        else:
            await nsfw_only(ctx)

    @commands.command(alias="bj")
    @dm_or_nsfw()
    async def blowjob(self, ctx: commands.Context):
        """Blowjob hentai"""
        source = ["bj", "blowjob"]
        if ctx.channel.type is discord.ChannelType.private or ctx.channel.is_nsfw():
            embed = await nekoslife(ctx, f"{source[randint(0, 1)]}")
            if embed is None:
                return await ctx.reply(":confused: Что-то пошло не так.")
            else:
                return await ctx.send("", embed=embed)
        else:
            await nsfw_only(ctx)

    @commands.command()
    @dm_or_nsfw()
    async def pwankg(self, ctx: commands.Context):
        """hentai pwankg"""
        if ctx.channel.type is discord.ChannelType.private or ctx.channel.is_nsfw():
            embed = await nekoslife(ctx, "pwankg")
            if embed is None:
                return await ctx.reply(":confused: Что-то пошло не так.")
            else:
                return await ctx.send("", embed=embed)
        else:
            await nsfw_only(ctx)

    @commands.command()
    @dm_or_nsfw()
    async def spank(self, ctx: commands.Context):
        """hentai spank"""
        if ctx.channel.type is discord.ChannelType.private or ctx.channel.is_nsfw():
            embed = await nekoslife(ctx, "spank")
            if embed is None:
                return await ctx.reply(":confused: Что-то пошло не так.")
            else:
                return await ctx.send("", embed=embed)
        else:
            await nsfw_only(ctx)

    @commands.command()
    @dm_or_nsfw()
    async def trap(self, ctx: commands.Context):
        """It's a trap!"""
        if ctx.channel.type is discord.ChannelType.private or ctx.channel.is_nsfw():
            embed = await nekoslife(ctx, "trap")
            if embed is None:
                return await ctx.reply(":confused: Что-то пошло не так.")
            else:
                return await ctx.send("", embed=embed)
        else:
            await nsfw_only(ctx)

    @dm_or_nsfw()
    @commands.command(aliases=["нсфв"])
    async def nsfw(self, ctx: commands.Context):
        """Пост из hot 25 r/nsfw"""
        if ctx.channel.type is discord.ChannelType.private or ctx.channel.is_nsfw():
            post = await reddit_radnom_hot(ctx, "nsfw")
            if type(post) is str:
                await ctx.send(post)
            elif type(post) is discord.Embed:
                await ctx.send("", embed=post)
            else:
                await something_went_wrong(ctx)
        else:
            await nsfw_only(ctx)
