import asyncio
import math

import discord
from .player import CustomPlayer
from discord.ext import commands
from setup import COLOR
from collections import deque
from utils.messages import reactions_add
from utils import sec_to_text
import wavelink


emoji_controls = ["⬅️", "➡️", "⏮️", "⏭️"]


class Playlist:
    def __init__(self, ctx: commands.Context, player: CustomPlayer) -> None:
        self.ctx = ctx
        self.player = player

    @staticmethod
    def total_duration(queue :wavelink.Queue) -> str:
        duration_sec = sum([track.length for track in queue])
        return sec_to_text(duration_sec)

    def get_embed(self, tracks, curr_page, pages):
        embed = discord.Embed(color=COLOR)
        embed.title = f"Треков в очереди: **{len(self.player.queue)}**  ({self.total_duration(self.player.queue)})"
        playlist_text = ""
        for index, track in enumerate(tracks):
            playlist_text += f"**{curr_page*10+index+1}.** [{track.title}]({track.uri}) заказан **{track.user.name}**  ({sec_to_text(track.length)})\n"
        embed.description = playlist_text
        if pages > 1:
            embed.set_footer(text=f"Page {curr_page+1}/{pages}")

        return embed

    async def paginator(self):
        curr_page = 0
        playlist = list(self.player.queue)
        pages = math.ceil(len(playlist)/10)
        message: discord.Message = None

        while True:
            tracks = playlist[curr_page*10: (curr_page+1)*10]
            embed = self.get_embed(tracks, curr_page, pages)

            if not message:
                message = await self.ctx.send("", embed=embed)
            else:
                await message.edit(content="", embed=embed)

            if pages > 1:
                await reactions_add(message, emoji_controls)
            else:
                break

            def check(reaction, user):
                return (
                    user == self.ctx.author
                    and str(reaction.emoji) in emoji_controls
                    and reaction.message.id == message.id
                )

            try:
                reaction, user = await self.ctx.bot.wait_for(
                    "reaction_add", timeout=60.0, check=check
                )
            except asyncio.TimeoutError:
                break

            if str(reaction.emoji) == "⬅️":
                curr_page = max(0, curr_page - 1)
            elif str(reaction.emoji) == "➡️":
                curr_page = min(pages - 1, curr_page + 1)
            elif str(reaction.emoji) == "⏮️":
                curr_page = 0
            elif str(reaction.emoji) == "⏭️":
                curr_page = pages - 1

            await message.remove_reaction(reaction.emoji, user)
        await message.delete(delay=60)
