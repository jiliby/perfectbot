from .music import Music
from discord.ext.commands import errors


async def setup(bot):
    """setup function"""
    from setup import config
    if None in [config["lavalink_node"], config["lavalink_pwd"]]:
        raise errors.ExtensionError(
            "Lavalink node connection configuration not found", name="cogs.music")
    await bot.add_cog(Music(bot))
