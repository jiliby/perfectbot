import asyncio
from functools import wraps
from math import ceil
from typing import Optional

import async_timeout
import discord
import requests
import sponsorblock as sb
import wavelink
from bs4 import BeautifulSoup
from discord.ext import commands
from wavelink import (WavelinkException, SoundCloudTrack,
                      YouTubePlaylist, YouTubeTrack)
from wavelink.ext import spotify
from wavelink.ext.spotify import SpotifyTrack

from setup import COLOR, config
from utils.http import is_url
from utils.messages import something_went_wrong

from .player import CustomPlayer
from .playlist import Playlist
from .search import Search


class MusicLoader():
    """helper class for loading music from different resources"""

    @classmethod
    async def spotify(cls,
                      ctx: commands.Context,
                      player: CustomPlayer,
                      url: str):
        if None in [config["spotify_id"], config["spotify_secret"]]:
            return
        info = requests.get(url)
        soup = BeautifulSoup(info.text, 'html.parser')
        title = soup.find('title').text
        if "track" in url:
            try:
                with async_timeout.timeout(20):
                    tracks = [await SpotifyTrack.search(query=url, return_first=True)]
            except (WavelinkException, asyncio.TimeoutError):
                return
        elif "album" or "playlist" in url:
            try:
                with async_timeout.timeout(60):
                    tracks = await SpotifyTrack.search(query=url)
            except (WavelinkException, asyncio.TimeoutError):
                return
        if not tracks or tracks == []:
            await something_went_wrong(ctx)
            return
        embed = cls.added_tracks(tracks, title, url, ctx.author)
        await ctx.send("", embed=embed)
        for track in tracks:
            setattr(track, 'user', ctx.author)
            player.queue.put(item=track)
        if not player.is_playing():
            await player.start()

    @classmethod
    async def start_from_url(cls,
                             ctx: commands.Context,
                             player: CustomPlayer,
                             url: str):
        """Аdd the songs from the URL to the queue and start playing (if nothing is playing right now)"""
        if "youtube.com/playlist" in url:
            tracks = await cls.load_YT_playlist(ctx, url)
        elif "youtu.be" in url or "youtube.com/watch?v" in url:
            tracks = await cls.load_YT_track(ctx, url)
        elif "soundcloud.com/" in url:
            tracks = await cls.load_soundcloud_track(ctx, url)
        else:
            await ctx.reply("Источник не поддерживается 🙃", delete_after=10)
            return
        if not tracks or tracks == []:
            await something_went_wrong(ctx)
            return
        for track in tracks:
            setattr(track, 'user', ctx.author)
            player.queue.put(item=track)
        if not player.is_playing():
            await player.start()

    @classmethod
    async def start_from_first(cls,
                               ctx: commands.Context,
                               player: CustomPlayer,
                               request: str):
        """
        Searching for tracks and adding the first found result to the queue and start playing (if nothing is playing right now)
        """
        track: list = await YouTubeTrack.search(request)
        track = track[0]
        setattr(track, 'user', ctx.author)
        embed = cls.added_tracks(
            [track], track.title, track.uri, ctx.author)
        await ctx.send("", embed=embed)
        player.queue.put(item=track)
        if not player.is_playing():
            await player.start()

    @classmethod
    async def start_from_search(cls,
                                ctx: commands.Context,
                                player: CustomPlayer,
                                request: str):
        """Search tracks and save the results until one is selected"""
        provider = YouTubeTrack
        tracks: list = await provider.search(request)
        search = Search(ctx, player)
        track = await search.results(tracks, request)
        if not track:
            return
        setattr(track, 'user', ctx.author)
        embed = cls.added_tracks(
            [track], track.title, track.uri, ctx.author)
        await ctx.send("", embed=embed)
        
        player.queue.put(item=track)
        if not player.is_playing():
            await player.start()
        

    @staticmethod
    def added_tracks(tracks, title, url, user) -> discord.Embed:
        """Генерирует embed для вывода добавленых в очередь треков"""
        embed = discord.Embed(color=COLOR, title=title, url=url)

        if len(tracks) > 1:
            embed.set_author(
                name=f"🎵 Добавлено в очередь {len(tracks)} треков")
        else:
            embed.set_author(name=f"🎵 Добавлено в очередь ")
        embed.title = title
        embed.url = url
        embed.set_footer(text=user.name,
                         icon_url=user.avatar.url)
        return embed

    @classmethod
    async def load_YT_track(cls, ctx: commands.Context, request: str):
        provider = YouTubeTrack
        try:
            with async_timeout.timeout(20):
                tracks = await provider.search(request)
                embed = cls.added_tracks(
                    [tracks[0]], tracks[0].title, request, ctx.author)
                await ctx.send("", embed=embed)
                return [tracks[0]]
        except (WavelinkException, asyncio.TimeoutError):
            return

    @classmethod
    async def load_YT_playlist(cls, ctx: commands.Context, request: str):
        provider = YouTubePlaylist
        try:
            with async_timeout.timeout(20):
                tracks = await provider.search(request)
                embed = cls.added_tracks(
                    tracks.tracks, tracks.name, request, ctx.author)
                await ctx.send("", embed=embed)
                return tracks.tracks
        except (WavelinkException, asyncio.TimeoutError):
            return

    @classmethod
    async def load_soundcloud_track(cls, ctx: commands.Context, request: str):
        provider = SoundCloudTrack
        try:
            with async_timeout.timeout(20):
                tracks = await provider.search(request)
                embed = cls.added_tracks(
                    [tracks[0]], tracks[0].title, request, ctx.author)
                await ctx.send("", embed=embed)
                return [tracks[0]]
        except (WavelinkException, asyncio.TimeoutError):
            return


class Music(commands.Cog):
    "Музыка! 🎶"

    def __init__(self, bot: commands.AutoShardedBot):
        self.bot: commands.AutoShardedBot = bot

    # -----------------------------------------------------------------------------------
    # cog event listeners
    @commands.Cog.listener()
    async def on_ready(self) -> None:
        """Create node in node pool"""
        if config["spotify_id"] and config["spotify_secret"]:
            spotify_client = spotify.SpotifyClient(
                client_id=config["spotify_id"],
                client_secret=config["spotify_secret"])
        else:
            spotify_client = None

        node: wavelink.Node = wavelink.Node(uri=config["lavalink_node"], password=config["lavalink_pwd"])
        await wavelink.NodePool.connect(client=self.bot, nodes=[node])

    @commands.Cog.listener()
    async def on_wavelink_node_ready(self, node: wavelink.Node) -> None:
        """Event fired when a node has finished connecting."""
        print(f'[Music]\tINFO: Node <{node.uri}> is ready!')

    @classmethod
    def is_requester(cls, user: discord.User,
                           track: wavelink.Playable) -> bool:
        return True if track.user.id == user.id else False

    @commands.Cog.listener()
    async def on_wavelink_track_start(self, payload: wavelink.TrackEventPayload):
        track: wavelink.Playable = payload.track
        player: CustomPlayer = payload.player
        sbClient = sb.Client()
        try:
            segments = sbClient.get_skip_segments(track.uri)
        except:
            return
        
        while True:
            if track != player.current:
                break
            currentPosition = player.position
            if segments:
                for segment in segments:
                    if segment.category == "music_offtopic" and currentPosition >= segment.start*1000 and currentPosition <= segment.end*1000:
                        await player.seek(segment.end*1000)
            await asyncio.sleep(0.5)
        
    @commands.Cog.listener()
    async def on_wavelink_track_end(self, payload: wavelink.TrackEventPayload):
        await payload.player.start()
       
    # -----------------------------------------------------------------------------------
    # utility functions
    @staticmethod
    async def connect(ctx: commands.Context) -> discord.VoiceProtocol:
        """returns player (voice_client), connects bot to the user's voice channel if it is not connected yet

        Args:
            ctx (commands.Context): message context

        Returns:
            discord.VoiceClient: guild voice client
        """
        if ctx.voice_client:
            return ctx.voice_client
        else:
            try:
                player: CustomPlayer = await ctx.author.voice.channel.connect(
                    cls=CustomPlayer)
            except (asyncio.TimeoutError or discord.ClientException):
                return
            return player

    def voice_check(f):
        """Wrap commands to check voice channels"""
        @wraps(f)
        async def exec(*args, **kwargs):
            # get message context from args
            ctx: commands.Context = args[1]
            if not ctx.author.voice:
                await ctx.reply(
                    "Необходимо находиться в голосовом канале", delete_after=10
                )
                return None
            if (ctx.author.voice and ctx.voice_client) and (ctx.author.voice.channel != ctx.voice_client.channel):
                await ctx.reply(
                    "Вы находитесь не в том голосовом канале", delete_after=10
                )
                return None
            else:
                return await f(*args, **kwargs)
        return exec

    # -----------------------------------------------------------------------------------
    # MUSIC cog commands

    @commands.command()
    @commands.guild_only()
    @voice_check
    async def play(self, ctx: commands.Context, *, request_string: str = ""):
        """Играть музыку с указанного <url> или попытаться найти на **youtube**")"""
        await ctx.message.delete(delay=20)
        if request_string == "":
            await ctx.reply(content="Введите название или ссылку на трек",
                            delete_after=10)
            return
        player: CustomPlayer = await self.connect(ctx)
        if not is_url(request_string):
            await MusicLoader.start_from_search(ctx=ctx, player=player, request=request_string)
        elif spotify.decode_url(request_string):
            await MusicLoader.spotify(ctx, player, request_string)
        else:
            await MusicLoader.start_from_url(ctx, player, url=request_string)
        request_string = ""

    @commands.command()
    @commands.guild_only()
    @voice_check
    async def playnow(self, ctx: commands.Context, *, request_string: str = ""):
        """
        При работе с url аналогична команде play.
        Если выполняется поиск трека - добавляет в очередь воспроизведения первый вариант из результатов.
        """
        await ctx.message.delete(delay=20)
        if request_string == "":
            await ctx.reply(content="Введите название или ссылку на трек",
                            delete_after=10)
            return
        player: CustomPlayer = await self.connect(ctx)
        if not is_url(request_string):
            await MusicLoader.start_from_first(ctx, player, request=request_string)
        elif spotify.decode_url(request_string):
            await MusicLoader.spotify(ctx, player, url=request_string)
        else:
            await MusicLoader.start_from_url(ctx, player, url=request_string)
        request_string=""

    @commands.command(aliases=["resume", "p"])
    @commands.guild_only()
    @voice_check
    async def pause(self, ctx: commands.Context):
        """Приостанавливает или возобновляет воспроизведение трека"""
        player: CustomPlayer = ctx.voice_client
        await ctx.message.delete()
        if player.is_playing():
            if player.is_paused():
                await player.pause()
            else:
                await player.resume()

    @commands.command()
    @commands.guild_only()
    @voice_check
    async def skip(self, ctx: commands.Context):
        """Пропуск текущего трека по результатам голосования или заказчиком"""
        await ctx.message.delete(delay=20)
        if ctx.voice_client is None:
            return await ctx.reply("Сейчас ничего не играет 🙃", delete_after=10)
        player: CustomPlayer = ctx.voice_client
        if ctx.author.guild_permissions.administrator or self.is_requester(
                ctx.author, player._source):
            await player.skip()
            return
        player.skip_votes.add(ctx.author.id)
        user_counter = len(
            [member for member in player.channel.members if not member.bot])
        votes_counter = len(player.skip_votes)
        required_votes = ceil(0.5 * user_counter)
        if votes_counter >= required_votes:
            await player.skip()
            return
        await ctx.send(
            f"{ctx.author.mention} проголосовал за пропуск ({votes_counter}/{required_votes})",
            delete_after=30,
        )
        await ctx.message.delete()

    @commands.command()
    @commands.guild_only()
    @voice_check
    async def np(self, ctx: commands.Context):
        """Что сейчас играет?"""
        await ctx.message.delete(delay=20)
        if ctx.voice_client is None:
            return await ctx.reply("Сейчас ничего не играет 🙃", delete_after=10)
        player: CustomPlayer = ctx.voice_client
        now_playing = player.now_playing()
        if not now_playing:  # second check if bot idle
            await ctx.reply("Сейчас ничего не играет 🙃", delete_after=10)
        else:
            await ctx.send("", embed=now_playing, delete_after=60)

    @commands.command(aliases=["q", "playlist"])
    @commands.guild_only()
    @voice_check
    async def queue(self, ctx: commands.Context):
        """Показать очередь воспроизведения"""
        await ctx.message.delete(delay=20)
        if ctx.voice_client is None:
            return await ctx.reply("Сейчас ничего не играет 🙃", delete_after=10)
        player: CustomPlayer = ctx.voice_client
        if player.queue.is_empty:
            return await ctx.reply("В очереди воспроизведения пусто.", delete_after=10)
        else:
            playlist = Playlist(ctx, player)
            await playlist.paginator()

    @commands.command(aliases=["cq"])
    @commands.guild_only()
    @voice_check
    async def clearqueue(self, ctx: commands.Context):
        """Очистить очередь воспроизведения"""
        if ctx.author.guild_permissions.administrator:
            player: CustomPlayer = ctx.voice_client
            player.queue.clear()
            await ctx.reply("Очередь воспроизведения очищена")
        else:
            await ctx.reply(
                "У тебя нет здесь власти, обратитесь к администратору",
                delete_after=10)
        await ctx.message.delete(delay=10)

    @commands.command(aliases=["rq"])
    @commands.guild_only()
    @voice_check
    async def removefromqueue(self,
                              ctx: commands.Context,
                              position: Optional[int] = 1):
        """
        Удалить трек из очереди по его [position] в списке. 
        По умолчанию удаляет следующий в очереди трек
        """
        player: CustomPlayer = ctx.voice_client
        try:
            track = player.queue[position - 1]
            if ctx.author.guild_permissions.administrator or self.is_requester(
                    ctx.author, track):
                del player.queue[position - 1]
                await ctx.reply(f"Удален трек **{track.title}**",
                                delete_after=10)
        except IndexError:
            await ctx.reply("Удалить что?", delete_after=10)
        except AttributeError:
            pass
        await ctx.message.delete(delay=10)

    @commands.command(aliases=["leave"])
    @commands.guild_only()
    async def stop(self, ctx: commands.Context):
        """Остановить воспроизведение"""
        if ctx.voice_client is None:
            return await ctx.reply("Сейчас ничего не играет 🙃", delete_after=10)
        if ctx.author.guild_permissions.administrator:
            player: CustomPlayer = ctx.voice_client
            await player.leave()
        else:
            await ctx.reply(
                "У тебя нет здесь власти, обратитесь к администратору",
                delete_after=10)
        await ctx.message.delete(delay=10)
