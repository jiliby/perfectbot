import asyncio

import async_timeout
import discord
from discord.ext import commands
import wavelink
from setup import COLOR


class CustomPlayer(wavelink.Player):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.queue = wavelink.Queue()
        
        
        self.skip_votes = set()

    async def leave(self) -> None:
        self.queue.clear()
        self.skip_votes = set()
        await self.stop()
        await self.disconnect()

    async def start(self) -> None:
        if self.is_playing():
            return

        try:
            with async_timeout.timeout(300):
                song = await self.queue.get_wait()
        except asyncio.TimeoutError:
            if not self.is_playing():
                await self.disconnect()
            return
        self.skip_votes = set()
        #self._current = song
        await self.play(song)

    async def handle_next(self):
        self.skip_votes = set()
        #self._current = None
        await self.start()

    async def skip(self) -> None:
        """Go to the next track"""
        # do not forget convert seconds to miliseconds --- fixed
        if self.current:
            await self.seek(self.current.length)

    def now_playing(self) -> discord.Embed:
        if not self.current:
            return None
        embed = discord.Embed(color=COLOR,
                              title=self.current.title,
                              url=self.current.uri)
        embed.set_author(name="🎵 Сейчас играет:")
        embed.set_footer(
            text=f"Добавлено пользователем: {self.current.user.name}",
            icon_url=self.current.user.avatar.url,
        )
        if self.current.thumbnail:
            embed.set_image(url=self.current.thumbnail)
        return embed
