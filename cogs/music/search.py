import asyncio

import discord
from discord.ext import commands

from setup import COLOR
from utils import sec_to_text
from utils.messages import reactions_add

from .player import CustomPlayer

emoji_control_list = ["1️⃣", "2️⃣", "3️⃣", "4️⃣", "5️⃣", "❌"]


class Search:
    def __init__(self, ctx: commands.Context, player: CustomPlayer) -> None:
        self.ctx = ctx
        self.player = player
        
    def get_embed(self, tracks, request) -> discord.Embed:
        embed = discord.Embed(color=COLOR)
        text: str = ""
        for i, entry in enumerate(tracks[:5]):
            text += f"**{i+1}.** [{entry.title}]({entry.uri})  {sec_to_text(entry.length)}\n"
        embed.add_field(name=f"Найдено по запросу: {request}", value=text)
        embed.set_footer(
            text="Выберите нужный вариант, или ❌ - что бы отменить поиск")
        return embed
    
    async def results(self, tracks, request):
        message: discord.Message = None
        embed = self.get_embed(tracks, request)
        message = await self.ctx.send("", embed=embed)
        await reactions_add(message, emoji_control_list)
        def check(reaction, user):
            return (
                user == self.ctx.author
                and str(reaction.emoji) in emoji_control_list
                and reaction.message.id == message.id
            )
        
        try:
            reaction, user = await self.ctx.bot.wait_for(
                "reaction_add", timeout=90.0, check=check
            )
        except asyncio.TimeoutError:
            await message.delete()
            return
        await message.remove_reaction(reaction.emoji, user)
        if str(reaction.emoji) == "❌":
            await message.delete()
            return
        elif str(reaction.emoji) == "1️⃣":
            track = tracks[0]
        elif str(reaction.emoji) == "2️⃣":
            track = tracks[1]
        elif str(reaction.emoji) == "3️⃣":
            track = tracks[2]
        elif str(reaction.emoji) == "4️⃣":
            track = tracks[3]
        elif str(reaction.emoji) == "5️⃣":
            track = tracks[4]
        await message.delete()
        return track