# import
from random import randint

import typing
import discord
from discord.ext import commands

from utils.http import nekoslife
from utils.messages import something_went_wrong


class Anime(commands.Cog):
    """
    Random images, gifs and other anime stuff
    """

    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def smug(self, ctx):
        """
        Get a random smug gif
        """
        embed = await nekoslife(ctx, "smug")
        if embed is None:
            await something_went_wrong(ctx)
        else:
            return await ctx.send("", embed=embed)

    @commands.command(aliases=["неко"])
    async def neko(self, ctx):
        """
        Get a random neko
        """
        embed = await nekoslife(ctx, "neko")
        if embed is None:
            await something_went_wrong(ctx)
        else:
            return await ctx.send("", embed=embed)

    @commands.command()
    async def anime(self, ctx):
        """
        Get a random anime image
        """
        embed = await nekoslife(ctx, "wallpaper")
        if embed is None:
            await something_went_wrong(ctx)
        else:
            return await ctx.send("", embed=embed)

    @commands.command()
    async def fox_girl(self, ctx):
        """
        Get a random fox girl
        """
        embed = await nekoslife(ctx, "fox_girl")
        if embed is None:
            await something_went_wrong(ctx)
        else:
            return await ctx.send("", embed=embed)



    # Команды с упоминанием юзера

    @commands.command(aliases=["вайфу"])
    async def waifu(self, ctx, user: typing.Optional[discord.Member]):
        """
        Get your waifu!
        """
        embed = await nekoslife(ctx, "waifu")
        if embed is None:
            return await something_went_wrong(ctx)
        elif user is None or user == ctx.author:
            text = ""
        else:
            text = f"Выбираем вайфу для {user.mention}"
        await ctx.send(text, embed=embed)

    @commands.command()
    async def poke(self, ctx, user: typing.Optional[discord.Member]):
        """
        Poke a user or just send a random gif
        """
        embed = await nekoslife(ctx, "poke")
        if embed is None:
            return await something_went_wrong(ctx)
        elif user is None or user == ctx.author:
            text = ""
        else:
            text = f"{ctx.author.mention} ткнул пальцем в {user.mention}"
        await ctx.send(text, embed=embed)

    @commands.command()
    async def hug(self, ctx, user: typing.Optional[discord.Member]):
        """
        Hug a user or just send a random gif
        """
        source = ["hug", "cuddle"]
        embed = await nekoslife(ctx, f"{source[randint(0, 1)]}")
        if embed is None:
            await something_went_wrong(ctx)
        elif user is None or user == ctx.author:
            text = ""
        else:
            text = f"{ctx.author.mention} крепко обнимает {user.mention}"
        await ctx.send(text, embed=embed)

    @commands.command()
    async def pat(self, ctx, user: typing.Optional[discord.Member]):
        """
        Pat a user or just send a random gif
        """
        embed = await nekoslife(ctx, "pat")
        if embed is None:
            await something_went_wrong(ctx)
        elif user is None or user == ctx.author:
            text = ""
        else:
            text = f"{ctx.author.mention} неожиданно решил погладить {user.mention}"
        await ctx.send(text, embed=embed)
