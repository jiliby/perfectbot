import os
from dotenv import load_dotenv

# set bot color
COLOR: int = 0x99CCCC


def config_load() -> dict:
    """Load enviroment variables from .env and OS

    Raises:
        RuntimeError: if unable load Discord bot token

    Returns:
        dicrt: config dictionary
    """
    load_dotenv()

    conf = {
        "token": os.getenv("BOT_TOKEN"),
        "google_api_key": os.getenv("SEARCH_API"),
        "custom_search_engine": os.getenv("SEARCH_ENGINE"),
        "lavalink_node": os.getenv("LAVALINK_NODE"),
        "lavalink_pwd": os.getenv("LAVALINK_PWD"),
        "spotify_id": os.getenv("SPOTIFY_ID"),
        "spotify_secret": os.getenv("SPOTIFY_SECRET")
    }

    if conf["token"] is None:
        raise RuntimeError(
            "Unable to import bot token from environment variables, bot cannot be started!")
    return conf


config: dict = config_load()
