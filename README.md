# PerfectBot

Your own Discord bot to replace all the others.


## How to try
You can freely use the bot by inviting it to your server through this [link](https://discord.com/api/oauth2/authorize?client_id=1012361714257379439&permissions=1641184357462&scope=bot)
## Run by yourself

Also, you can run your own instance of the bot, but before you start, you need to follow these steps:
1. Go to the [Discord Developer Portal](https://discord.com/developers/applications) and create a new application.
2. Go to the "Bot" section of the application and create a new bot.
3. Get the bot token that will be used later.
4. Invite the bot to your Discord server using the following link (replace `<CLIENT_ID>` with your application's client ID): `https://discord.com/api/oauth2/authorize?client_id=<CLIENT_ID>&permissions=8&scope=bot`

Additional steps (optional) to enable full bot functionality:
1. Create Google Programmable Search Engine and get API key and Search Engine ID. Required for the `Search` module to work. For more information on how to obtain these keys, please refer to [this link](https://developers.google.com/custom-search/v1/introduction).
2. [Lavalink](https://github.com/freyacodes/Lavalink) node for the functionality of the `Music` module. You can create your own or use one of the free public nodes from [here](https://lavalink.darrennathanael.com/).

### Requirements
You can run the bot directly using `Python` or launch in the Docker container. 
To launch directly you need `Python` version `3.8` or higher.
To launch into Docker container you need install Docker desktop or Docker Engine (with Docker Compose for easy perfectbot+lavalink installation)


### Іnstallation

To run the bot locally, you need to do the following:

```bash
# Clone this repository
git clone https://gitlab.com/jiliby/perfectbot/

# Navigate to the repository directory
cd perfectbot
```

Next, you need to create and fill in the environment file .env as specified below:
```.env
#discord bot token
BOT_TOKEN=<bot_token>

#Google Programmable Search Engine (optional)
SEARCH_API=<API_key>
SEARCH_ENGINE=<engine_ID>

#lavalink node (optional)
LAVALINK_NODE=<url_or_ip>:<port>
LAVALINK_PWD=<node_password>
```

## Running directly

1. Install requirements

```bash
python3 -m pip install -U -r requirements.txt
```

2. Run your bot
```bash
python3 run.py
```

## Running in a container

1. Get a image. You can build own image using Dockerfile (1.1) or use already builded image (1.2)

    1.1. Building own image.
    You can build image by running this command into project directory:
    ```bash
    docker build -t <your_image_name> -f Dockerfile .
    ```

    1.2. Pull image
    You can use already builded image by me from [Dockerhub](https://hub.docker.com/r/jiliby/perfectbot)
    ```bash
    docker pull jiliby/perfectbot
    ```

2. Run

    2.1. For you own builded image
    ```bash run --env-file ./.env <your_image_name>
    docker container run PerfectBot
    ```

    2.2. For prebuid image
    ```bash
    docker run --env-file ./.env jiliby/perfectbot
    ```

    2.3. Run perfectbot+lavalink using Docker compose
    ```bash
    docker compose up -d
    ```