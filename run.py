import asyncio
import os
import discord
from discord.ext import commands
from pretty_help import PrettyHelp

from setup import config, COLOR


def get_intends():
    intents = discord.Intents.default()
    intents.members = True
    intents.message_content = True
    return intents


async def load_ext(bot: commands.AutoShardedBot):
    """Загружает модули расширений из каталога cogs"""
    counter = 0  # счётчик загруженных расширений
    for ext in os.listdir("cogs"):
        if os.path.isdir("cogs/" + ext) and not ext.startswith("__"):
            try:
                await bot.load_extension(f"cogs.{ext}")
                counter += 1
            except Exception as e:
                print(
                    f"[Main]\tError: {ext} - loading failed\n[Main]\tError: {type(e).__name__}: {e}")
    print(f"[Main]\tINFO: {counter} extensions loaded successfully.")


async def main():
    # задаём префикс, команду "help" и цвет для embedded
    bot = commands.AutoShardedBot(
        command_prefix=".",
        help_command=PrettyHelp(active_time=120, color=COLOR),
        intents=get_intends()
    )

    await load_ext(bot)

    # events
    @bot.event
    async def on_ready():
        await bot.change_presence(
            status=discord.Status.online, activity=discord.Game(".help")
        )
        print("[Main]\tINFO: Bot is online!")

    # bot run
    async with bot:
        await bot.start(config["token"])


if __name__ == "__main__":
    asyncio.run(main())
