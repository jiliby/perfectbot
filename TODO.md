# PerferctBot

Your own Discord bot to replace everyone else

### Todo

- [ ] update the readme to the latest information
- [ ] database integration
  - [ ] mongodb
  - [ ] others (???)
- [ ] multilang support (EN, UA, RU)
    - [ ] separate lines of text into separate files
    - [ ] translate all strings to the required localizations
      - [ ] english
      - [ ] україньська
      - [ ] русский
    - [ ] implement the choice of language used for the discord guid
    - [ ] implement the use of the selected localization