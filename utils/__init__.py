def execute_time(func):
    import time

    def exec(*args, **kwargs):
        start = time.time()
        result = func(*args, **kwargs)
        end = time.time()
        print(f"[*] Execution time: {end-start} seconds.")
        return result

    return exec


def sec_to_text(time: int|float) -> str:
    """Convert time in miliseconds to human readable string

    Args:
        seconds (int, float): time in seconds

    Returns:
        str: time in readable string
    """
    seconds = int(time//1000)
    if seconds > 3600:  # if hours
        return f"{int(seconds // 3600)}h {int(seconds % 3600 // 60)}m {int(seconds % 60)}s"
    elif seconds > 60:  # if minutes
        return f"{int(seconds // 60)}m {int(seconds % 60)}s"
    else:  # if seconds
        return f"{int(seconds)}s"