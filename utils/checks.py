import discord
from discord.ext import commands


def dm_or_nsfw():
    """NSFW commands should work only for DM or NSFW channels"""
    async def predicate(ctx: commands.Context):
        if ctx.channel.type is discord.ChannelType.private or ctx.channel.is_nsfw():
            return True
        else:
            return False
    return commands.check(predicate)
