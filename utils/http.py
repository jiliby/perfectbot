import json
import re
from dataclasses import dataclass
from random import randint

import aiohttp
import discord
import requests
from discord.ext import commands

from setup import COLOR

header = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36"
}

url_regex = re.compile(
    "http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+"
)


def is_url(url: str) -> bool:
    """Проверяет, является ли полученная строка url-адресом

    Args:
        url (str): string

    Returns:
        bool: string is a url?
    """
    return url_regex.match(url) is not None

@dataclass
class Response:
    url: str
    code: int
    text: str


def get_requests(url: str)-> Response:
    """
    Сделать запрос на указанный url с использованием requests
    """
    content = requests.get(url, headers=header)
    return Response(content.url, content.status_code, content.text)


async def get_aiohttp(url: str):
    """
    Сделать запрос на указанный url с использованием aiohttp
    Возвращает словарь:
        'url' : content.url,
        'code': content.status_code,
        'text': content.text
    """
    async with aiohttp.ClientSession(headers=header) as session:
        async with session.get(url) as content:
            return Response(content.url, content.status, await content.text())


async def head_aiohttp(url):
    """
    Сделать HEAD запрос на указанный url с использованием aiohttp
    Возвращает словарь:
        'url' : content.url
        'code': content.status_code,
        'text': content.text
    """
    async with aiohttp.ClientSession(headers=header) as session:
        async with session.head(url) as content:
            return Response(content.url, content.status, await content.text())


def head_requests(url):
    """
    Сделать HEAD запрос на указанный url с использованием requests
    Возвращает словарь:
        'url' : content.url,
        'code': content.status_code,
        'text': content.text
    """
    content = requests.head(url, headers=header)
    return Response(content.url, content.status_code, content.text)



async def randomimage(ctx: commands.Context, entity):
    """достаём картинки с randomimage"""
    content = await get_aiohttp(f"https://some-random-api.ml/img/{entity}")
    if content.code != 200:
        return await ctx.reply(":confused: Сервис не отвечает.")
    res = json.loads(content.text)
    try:
        res["link"]
    except:
        print(f"Error in:\n {content.text}\n\n")
        return await ctx.reply(":confused: Что-то пошло не так.")
    em = discord.Embed(color=COLOR)
    em.set_image(url=res["link"])
    await ctx.send("", embed=em)


async def nekoslife(ctx: commands.Context, part):
    """достаём картинки из nekoslife"""
    content = await get_aiohttp(f"https://nekos.life/api/v2/img/{part}")
    if content.code != 200:
        return await ctx.reply(":confused: Сервис не отвечает.")
    res = json.loads(content.text)
    try:
        res["url"]
    except:
        print(f"Error in:\n {content.text}\n\n")
        return None
    embed = discord.Embed(color=COLOR)
    embed.set_image(url=res["url"])
    return embed


async def reddit_radnom_hot(ctx: commands.Context, sub_r: str) -> str | int | discord.Embed:
    """Get random post from hot 25 subreddit (asynchronous)

    Args:
        ctx (commands.Context): context
        sub_r (str): subreddit

    Returns:
        str | discord.Embed: embed with post or url
    """
    content = await get_aiohttp(f'https://www.reddit.com/r/{sub_r}/hot.json?sort=hot')
    if content.code != 200:
        return content.code
    data = json.loads(content.text)
    url = data['data']['children'][randint(0, 25)]['data']['url']
    if url.lower().endswith(('.png', '.jpg', '.jpeg', '.gif', '.webp')):
        # если контент - картинка, то создаём Embed
        embed = discord.Embed(color=COLOR)
        embed.set_image(url=url)
        return embed
    else:  # если это не картинка, то оправляем просто ссылку
        return url
