import discord
from discord.ext import commands


async def reactions_add(msg: discord.Message, reactions):
    """Ставит реакции к сообщению"""
    for react in reactions:
        try:
            await msg.add_reaction(react)
        except discord.DiscordException:
            pass


async def something_went_wrong(ctx: commands.Context):
    """Пишем сообщение о ошибке"""
    await ctx.reply(":confused: Что-то пошло не так.", delete_after=15)


async def nsfw_only(ctx: commands.Context):
    """
    Пишем сообжение что команда NSFW-only
    (после чего и сообщение и команда будут удалены)
    """
    await ctx.reply(
        "Эту команду можно искользовать только в NSFW-каналах",
        delete_after=10)
    await ctx.message.delete(delay=10)
